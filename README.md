# SimpleID Authentication
This is a serverless project that does authentication with SimpleId (https://www.simpleid.xyz/).
You can set your own variables (API keys, developer ID, etc)  and deploy it yourself. 

# Deployment
`sls deploy`

# CORS
Theye are left wide open on this deployment, so you should tailor the code for your needs
in this regard. 

# Environment Variables: 
- `SIMPLEID_API_KEY`
- `SIMPLEID_APP_ORIGIN`
- `ALLOWED_ORIGINS`, i.e. `'["http://localhost:*"]'`