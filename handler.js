'use strict';
const auth = require('simpleid-node-sdk'); //require package

const appObj = {
  apiKey: process.env.SIMPLEID_API_KEY, //found in your SimpleID account page
  devId: process.env.SIMPLEID_DEV_ID, //found in your SimpleID account page
  authProviders: ['blockstack'], //array of auth providers that matches your modules selected
  storageProviders: ['blockstack'], //array of storage providers that match the modules you selected
  scopes: ['publish_data', 'store_write', 'email'] //array of permission you are requesting from the user
}

const ALLOWED_ORIGINS = process.env.ALLOWED_ORIGINS; 

console.log('ALLOWED ORIGINS = ', ALLOWED_ORIGINS)

/* Handle regex stuff for allowed CORS */
const makeRegex = function(input) {
  const specialChars = "\\.+?{}()[]^$";

  for(let c of specialChars) input = input.replace(c, "\\"+c);
  return new RegExp("^" + input.replace('*', '.*'));
}

const ALLOWED_DAPP_LOCATIONS 
  = (JSON.parse(ALLOWED_ORIGINS))
      .map(s => makeRegex(s));

function isValidCorsAddress(origin) {
  return ALLOWED_DAPP_LOCATIONS.find(r => r.test(origin));
}


/**
 * event.body = {
 *  id: 
 *  password:  
 *  hubUrl: 
 * 
 * }`
 */
module.exports.login = (event, context, callback) => {

  try {
    let credObj = JSON.parse(event.body); 
    console.log('Attempting to login: ', credObj.id)
    const origin = event.headers.origin
    console.log('headers = ', event.headers)
    console.log('origin = ', origin)
    if (!isValidCorsAddress(origin)){
      console.log(`Error: Invalid origin: ${origin} only ${ALLOWED_ORIGINS} is allowed.`)
      callback(new Error('Forbidden origin request.'))
    }
    let obj = Object.assign({}, appObj, {appOrigin: origin}); 
    const params = {
      credObj, 
      appObj: obj,
      userPayload: {}
    }
    auth.login(params).then(signin =>{
      console.log('signin object = ', signin); 
      callback(null, {
        statusCode: 200, 
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(signin)
      })
    }).catch(e => {
      console.log('Got an error when attempting to login: ', e)
      callback(e); 
    })

  } catch (e) {
    console.log('Received an errow when attempting to log a user in: ', e)
    callback(e) ; 
  }
};

module.exports.signup = (event, context, callback) => {

  try {
    let credObj = JSON.parse(event.body); 
    console.log('Attempting to create user: ', credObj.id)
    const origin = event.headers.origin; 
    console.log('origin = ', origin)
    if (!isValidCorsAddress(origin)){
      console.log(`Error: Invalid origin: ${origin} only ${ALLOWED_ORIGINS} is allowed.`)
      callback(new Error('Forbidden origin request.'))
    }
    let obj = Object.assign({}, appObj, {appOrigin: origin}); 
    auth.createUserAccount(credObj, obj).then(signin =>{
      console.log('signin object: ', signin)
      callback(null, {
        statusCode: 200, 
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(signin)
      })
    }).catch(e => {
      console.log('Got an error when attempting to create user: ', e)
      callback(e); 
    })

  } catch (e) {
    console.log('Received an errow when attempting to create a user: ', e)
    callback(e) ; 
  }
};

